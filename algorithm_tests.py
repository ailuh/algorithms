import unittest
import os
from importlib import import_module
import sys
from optparse import OptionParser
import inspect

names = os.listdir()
names.remove(os.path.basename(__file__))
sort_modules = []
for n in names:
    if n.endswith('.py'):
        if n.endswith('sort.py'):
            sort_modules.append(import_module(n[:-3]))


class TestStringMethods(unittest.TestCase):

    def test_sorting_algorithms(self):
        for sort in sort_modules:
            self.assertEqual([1,2,2,3,4,5,6,7,8,9], sort.sort([3,2,5,7,1,2,4,9,6,8]))

if __name__ == '__main__':
    unittest.main()
