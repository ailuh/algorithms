def sort(arr):
    i = 0
    while i < len(arr):
        key = arr[i]
        index = i
        for j in range(i, len(arr)):
            if key > arr[j]:
                key = arr[j]
                index = j
        arr[index] = arr[i]
        arr[i] = key
        i += 1
    return arr

